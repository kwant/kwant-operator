from collections import defaultdict
import functools as ft
import numpy as np
from cmath import exp

from hilbert_space import SiteArray
from kwoperator import Sum, Symmetrized, FiniteSupport


class KSpaceAdapter:
    """
    Attributes
    ----------
    sym : TranslationalSymmetry
    basis : SiteArray
        A single basis for the left and right spaces.
        Only contains sites from the fundamental domain
        of 'sym'.
    ts : sequence of ndarray
        One translation group element associated with
        each component operator.
    component_operators : sequence FiniteSupport
        The components of the k-space operator.
        The left and right basis of each component must be
        equal to 'basis'.
    """

    def __init__(self, sym, basis, components):
        self.sym = sym
        self.basis = basis
        self.ts, self.component_operators = zip(*components)
        assert all(c.left_basis == basis and c.right_basis == basis
                   for c in self.component_operators)

    def __call__(self, k, *, sparse=True):
        """Return the k-space operator at a given k-point."""
        values = [op(sparse=sparse) for op in self.component_operators]
        # NOTE: This can probably be vectorized over 'k'
        return sum(exp(1j * np.dot(k, t)) * v for t, v in zip(self.ts, values))


def k_space_adapter(op):
    """Return a representation of the operator in k-space

    Parameters
    ----------
    op : Symmetrized or sum of same
        Operator(s) symmetrized with respect to some symmetry group.
        The symmetry group for each component of the sum must be the same.

    Returns
    -------
    k_space_adapter: KSpaceAdapter
    """
    # NOTE: This insane set of 'isinstance' checks probably doesn't scale;
    #       is there a way we can do this with duck typing?
    ops = as_sum(op).operators
    if any(not isinstance(op, Symmetrized) for op in ops):
        raise TypeError('This adapter only works for symmetrized operators.')
    # NOTE: Cannot yet handle operators with commensurate symmetries
    sym = ops[0].group
    if any(op.group != sym for op in ops):
        raise ValueError('This adapter requires the symmetries of all '
                         'operators to be equal.')

    # Extract the underlying FiniteSupport operators
    ops = [op.operator for op in ops]

    # NOTE: Cannot yet handle projected operators
    if any(not isinstance(op, FiniteSupport) for op in ops):
        raise TypeError('This adapter only works with FiniteSupport operators')
    # NOTE: Cannot yet handle operators between different spaces
    space = ops[0].left_space
    if any(op.left_space != space or op.right_space != space for op in ops):
        raise ValueError('This adapter only works for operators in '
                         'a single Hilbert space.')

    # Decompose operators into bits for each symmetry group element.
    components = flatten(decompose_by_symmetry(sym, op) for op in ops)

    # Bring all the component operators into a common basis that only
    # contains sites in the FD of the symmetry.
    t, ops = zip(*components)
    basis, ops = with_common_basis(ops, sym)
    assert basis[0] == basis[1]
    basis = basis[0]
    components = list(zip(t, ops))

    return KSpaceAdapter(sym, basis, components)


def as_sum(operator):
    """Return the operator as a Sum operator"""
    return operator if isinstance(operator, Sum) else Sum(operator)


def _relevant_parameters(parameter_names, arguments):
    return {k: v for k, v in arguments.items() if k in parameter_names}


def _make_slices(sequence_of_sequences):
    """Return slices for indexing 'flatten(sequence_of_sequences)'."""
    offsets = [0] + list(np.cumsum([len(s) for s in sequence_of_sequences]))
    return [slice(*s) for s in zip(offsets, offsets[1:])]


def _unify(tags):
    """Return all unique tags, and index arrays into the original tag sequences."""
    slices = _make_slices(tags)
    all_tags = np.vstack(tags)
    unique_tags, inv = np.unique(all_tags, axis=0, return_inverse=True)
    return unique_tags, [inv[slice] for slice in slices]


def with_common_basis(ops, sym=None):
    """Return copies of 'ops' with a common basis.

    Parameters
    ----------
    ops : sequence of FiniteSupport
        The operators for which to find a common basis.

    Returns
    -------
    (basis, ops)
    """
    space = ops[0].left_space
    assert all(isinstance(op, FiniteSupport)
               and op.left_space == space
               and op.right_space == space
               for op in ops)

    tfm = sym.to_fd if sym else lambda x: x


    basis_tags, maps = _unify(flatten([
        (tfm(op.left_basis.tags), tfm(op.right_basis.tags))
        for op in ops
    ]))
    row_maps, col_maps = maps[::2], maps[1::2]
    basis = 2 * (SiteArray(space, basis_tags),)

    return basis, [
        FiniteSupport(basis, row_map[op.rows], col_map[op.cols], op.values)
        for op, row_map, col_map in zip(ops, row_maps, col_maps)
    ]


def decompose_by_symmetry(sym, op):
    """Decompose 'op' into components that connect different unit cells of 'sym'

    Parameters
    ----------
    sym : TranslationGroup
    op : FiniteSupport

    Returns
    -------
    decomposed: sequence of (ndarray, FiniteSupport)
        The components of 'op' that connect unit cells related by the given
        translation.
    """
    assert (isinstance(op, FiniteSupport)
            and op.left_space == op.right_space
            and op.left_space == sym.space)
    # Decompose a sum of translationally invariant operators into
    # components that connect different unit cells.
    to_tags = op.left_basis.tags[op.rows]
    from_tags = op.right_basis.tags[op.cols]
    translations = sym.which(to_tags) - sym.which(from_tags)

    by_symmetry = defaultdict(list)
    for i, t in enumerate(translations):
        by_symmetry[tuple(t)].append(i)

    return [
        (t,
         FiniteSupport(op.basis, op.rows[idxs], op.cols[idxs],
                       op.values if callable(op.values) else op.values[idxs])
        ) for t, idxs in by_symmetry.items()
    ]


def flatten(iter):
    return [x for xs in iter for x in xs]
