# Kwant operator interface

This repo is for hashing out ideas for the new interface to Kwant. The
fundamental idea is to represent everything as "operators" that act on
the same Hilbert space.

The different concepts are split into several modules in this repo.

#### `hilbert_space.py`
Definitions of Hilbert spaces and Sites.

#### `kwoperator.py`
Operators on Hilbert spaces, and groups of operators representing
symmetries. "Systems" (defined by a Hamiltonian operator) are constructed
by combining constituent operators.

#### `adapters.py` and `solvers.py`
Adapters take operators and produce datastructures that can be directly used
by solvers. We recognize that not all solvers require data in the same format


## Examples

#### `example.ipynb`
Basic example showing construction of Hilbert spaces and operators.

#### `k-space-example.ipynb`
A basic example showing the definition of a system with a 2D translational
symmetry, and the calculation of its bandstructure
