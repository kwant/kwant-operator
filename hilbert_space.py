from math import inf
import numpy as np
import itertools as it
import functools as ft
import operator
import abc
import weakref


class SiteArray:
    __slots__ = ('space', 'tags')

    def __init__(self, space, tags):
        self.space = space
        self.tags = tags = np.atleast_2d(np.array(tags, int))
        if tags.ndim != 2:
            raise ValueError("Expecting a 2-dimensional array-like for 'tags'.")
        sfs = space.finite_shape
        snid = space.num_inf_dims
        if tags.shape[1] != snid + len(sfs):
            raise ValueError(f"Expecting tags of length {snid + len(sfs)}.")
        for col, size in zip(tags.T[snid:], sfs):
            if np.amin(col) < 0 or np.amax(col) >= size:
                raise ValueError("Tag components out of bounds.")

    def __repr__(self):
        return 'SiteArray({0},\n{1})'.format(repr(self.space), repr(self.tags))

    def __eq__(self, other):
        return (isinstance(other, SiteArray)
                and self.space == other.space
                and np.array_equal(self.tags, other.tags))

    def __len__(self):
        return len(self.tags)

    def __getitem__(self, spec):
        return SiteArray(self.space, np.atleast_2d(self.tags[spec]))


class HilbertSpace(metaclass=abc.ABCMeta):
    @abc.abstractproperty
    def components(self):
        pass

    @abc.abstractproperty
    def names(self):
        pass

    @abc.abstractproperty
    def num_inf_dims(self):
        pass

    @abc.abstractproperty
    def finite_shape(self):
        pass

    def __hash__(self):
        return hash(self.names)

    def __eq__(self, other):
        # The following works because Hilbert spaces with the same name are
        # enforced to be equal.
        return isinstance(other, HilbertSpace) and self.names == other.names

    def __mul__(self, other):
        if not isinstance(other, HilbertSpace):
            raise TypeError("Cannot multiply Hilbert space by "
                            f"'{type(other).__name__}' instance.")
        return Product(*self.components, *other.components)

    def has_subspace(self, other):
        return _is_subsequence(other.names, self.names)

    def pos(self, sites):
        """Return default positions of given sites."""
        prim_vecs, inf_indices = self.infinite_basis
        inf_pos = sites.tags[:, inf_indices] @ prim_vecs

        fin_pos = np.zeros_like(inf_pos, dtype=float)
        for n, c in enumerate(self.components[self.num_inf_dims:]):
            if c.basis is None:
                continue
            fin_pos += c.basis[sites.tags[:, n + self.num_inf_dims]]

        if self.origin is not None:
            return inf_pos + fin_pos + self.origin
        else:
            return inf_pos + fin_pos


class Simple(HilbertSpace):
    _registry = weakref.WeakValueDictionary()

    def __init__(self, size, name, basis=None):
        if size == inf:
            self._size = inf
        else:
            self._size = int(size)
            if self._size != size:
                raise TypeError("The size of a simple Hilbert space must be"
                                "an integer or inf.")

        # Rafal: I am not happy with the code below but today ain't a day
        # to fix it. It works.
        if basis is None:
            # basis is None -> nothing to do
            self._basis = basis
        elif size == inf:
            # ifinite space -> basis must be a single vector
            basis = np.array(basis)
            if basis.ndim != 1:
                raise ValueError('For an infinite Hilbert space "basis" must '
                                 'be a single vector.')
            self._basis = basis
        else:
            # finite space -> basis must be 2d array-like and number of
            # vectors must match size of the Hilbert space
            basis = np.array(basis)
            if basis.ndim != 2:
                raise ValueError('For a finite Hilbert space "basis" must be a '
                                 '2d array-like object.')
            elif len(basis) != size:
                raise ValueError('Number of vectors in "basis" must match the '
                                 '"size" of Hilbert space.')
            self._basis = basis

        if not isinstance(name, str):
            raise TypeError(
                "The name of a simple Hilbert space must be a string.")
        self._name = name

        other = Simple._registry.setdefault(name, self)
        if other._size != size:
            raise ValueError("A different simple Hilbert space with the same"
                             " name already exists.")

    @property
    def components(self):
        return (self,)

    @property
    def names(self):
        return (self._name,)

    @property
    def basis(self):
        return self._basis

    @property
    def num_inf_dims(self):
        return int(self._size == inf)

    @property
    def finite_shape(self):
        return () if self._size == inf else (self._size,)

    def __repr__(self):
        return "{}({})".format(
            self.__class__.__name__,
            ", ".join([
                repr(self._size), '"{}"'.format(self._name), repr(self.basis)
            ])
        )


def _check_prim_vecs(prim_vecs):
    if prim_vecs.shape[0] > prim_vecs.shape[1]:
        raise ValueError('Number of primitive vectors exceeds '
                         'the space dimensionality.')

    if np.linalg.matrix_rank(prim_vecs) < len(prim_vecs):
        raise ValueError('Primitive vectors must be linearly independent.')


class Product(HilbertSpace):
    """Direct Product of simple Hilbert spaces."""
    def __init__(self, *components, origin=None):
        n = len(components)
        if n < 2:
            raise ValueError('Must provide at least 2 components for a '
                             'product space.')
        if n != len(set(components)):
            raise ValueError('Direct product may not contain multiple copies '
                             'of the same Hilbert space.')

        shape = []
        ninf = 0
        for c in components:
            if not isinstance(c, Simple):
                raise ValueError('Only products of simple Hilbert spaces are '
                                 'allowed.')
            if c._size != inf:
                shape.append(c._size)
            else:
                if shape:
                    raise ValueError('Infinite Hilbert spaces must come '
                                     'before finite ones in a product.')
                ninf += 1

        self._shape = tuple(shape)
        self._ninf = ninf
        self._components = components
        self.origin = origin

        # We need to check if real space dimension of all vectors is the same
        space_dims = [np.atleast_2d(c.basis).shape[1]
                      for c in components
                      if c.basis is not None]

        # We cannot have more than 1 unique dimensions.
        # 0 is okay as it is not required to have basis defined at all
        if len(set(space_dims)) > 1:
            raise ValueError("Dimension of real space must be the same for "
                             "all components.")

        # For infinite components in the hilbert space we need to check if
        # basis is linearly independent.
        prim_vecs, _ = self.infinite_basis
        if prim_vecs is not None:
            _check_prim_vecs(prim_vecs)

    @property
    def components(self):
        return self._components

    @property
    def names(self):
        return tuple(c._name for c in self._components)

    @property
    def num_inf_dims(self):
        return self._ninf

    @property
    def finite_shape(self):
        return self._shape

    @property
    def infinite_basis(self):
        """Return prim_vecs and indices of corresponding components."""
        components = [c for c in self.components[:self.num_inf_dims]
                      if c.basis is not None]

        if components:
            inf_indices, inf_basis = zip(*[
                (n, c) for n, c in enumerate(components)
            ])
            prim_vecs = np.row_stack([c.basis for c in inf_basis])
        else:
            inf_indices, prim_vecs = None, None

        return prim_vecs, inf_indices

    def __repr__(self):
        return "".join([self.__class__.__name__,
                        "(",
                        ", ".join(repr(c) for c in self._components),
                        ")"])


def _is_subsequence(x, y):
    it = iter(y)
    return all(c in it for c in x)

def _where_subsequence(x, y):
    it = enumerate(y)
    r = np.zeros(len(y), bool)
    for xx in x:
        for i, yy in it:
            if xx == yy:
                r[i] = True
                break
    return r
