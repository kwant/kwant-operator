import kwant

import kwoperator as kw
import adapters

lat = kwant.lattice.square()

T = kwant.TranslationalSymmetry((1, 0), (0, 1))

# Build bulk
H0 = kw.FiniteSupport.from_list([lat(0, 0)], [4])
H0 = kw.TranslationallyInvariant(T, H0)

V = kw.FiniteSupport.from_list([(lat(1, 0), lat(0, 0)),
                                (lat(0, 1), lat(0, 0))],
                               [-1, 1])
V = kw.TranslationallyInvariant(T, V)
H_2d = H0 + V + V.dagger()

# Build perturbation
H_perturb = kw.FiniteSupport.from_list(
    [lat(i, j) for i in range(3) for j in range(3)],
    [1 for _ in range(9)],
)

# "attach" perturbation to 2D lead
H_syst = H_2d + H_perturb

# Create low-level input for the 2D translationally invariant
# Green's function calculation
gf2d_input = adapters.greens_function(H_2d)

# Create low-level input for the 2D Green's function calculation with impurity
gf2d_input, impurities = adapters.greens_function(H_syst)
