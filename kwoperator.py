import abc
import itertools
import inspect
import scipy
import numpy as np
from operator import attrgetter
import scipy.sparse as sp

import hilbert_space
import lll

class Operator(metaclass=abc.ABCMeta):
    """Base class for all operators."""

## Simple Operators
#  operators that cannot be "broken down" any further

class SimpleOperator(Operator):
    """Base class for operators that directly specify matrix elements."""


class Identity(SimpleOperator):
    """The identity operator for a Hilbert space

    Attributes
    ----------
    space : HilbertSpace
    """

class FiniteSupport(SimpleOperator):
    """Operator with finite support.

    Private Attributes
    ------------------
    basis : pair of SiteArray
        The left/right bases for the operator
    rows, cols : ndarray of int
        Row/column indices for each of the matrix elements.
    values : callable or ndarray of complex
        The matrix elements. If a callable, then should accept 2
        'SiteArray's of the same length, and return an ndarray of
        complex of the same length.
    """

    def __init__(self, basis, rows, cols, values):
        self.basis = basis
        self.rows = np.asarray(rows, int)
        self.cols = np.asarray(cols, int)
        self.values = np.asarray(values, complex)

    def __call__(self, *, sparse=True):
        """Return the operator as a COO matrix or numpy array."""
        shape = (len(self.left_basis), len(self.right_basis))

        r = sp.coo_matrix((self.values, (self.rows, self.cols)),
                          shape=shape, dtype=complex)

        return r if sparse else r.todense()

    @property
    def left_basis(self):
        return self.basis[0]

    @property
    def left_space(self):
        return self.left_basis.space

    @property
    def right_basis(self):
        return self.basis[1]

    @property
    def right_space(self):
        return self.right_basis.space

    @staticmethod
    def from_list(to_sites, from_sites, values):
        """Construct a FiniteSupport operator from lists of matrix elements."""
        if to_sites.space != from_sites.space:
            raise TypeError('The to- and from- Hilbert spaces must be the same')
        if len(to_sites) != len(from_sites):
            raise ValueError('To- and from- SiteArrays must have same length')
        values = np.asarray(values)
        if values.shape != (len(to_sites),):
            raise ValueError('Values must be an array of scalars')

        # Remove duplicates and calculate row/column indices
        to_tags, rows = np.unique(to_sites.tags, axis=0, return_inverse=True)
        from_tags, cols = np.unique(from_sites.tags, axis=0, return_inverse=True)
        basis = (hilbert_space.SiteArray(to_sites.space, to_tags),
                 hilbert_space.SiteArray(from_sites.space, from_tags))

        return FiniteSupport(basis, rows, cols, values)


class FiniteProjector(SimpleOperator):
    """Diagonal projector onto a finite number of sites

    Attributes
    ----------
    sites : SiteArray
    """


## Composite Operators
#  operators that combine simple (and combinations of simple) operators
#  Or are these "wrappers"?

class Sum(Operator):
    """A sum of operators.

    Attributes
    ----------
    operators : tuple of Operator
    """
    def __init__(self, *operators):
        self.operators = operators


class Translated(Operator):
    """An operator translated by a translation group element.

    Given an operator 'A' and a translation group element 't',
    this represents the product 'tA'.
    """

    def __init__(self, translation, operator):
        self.translation = translation
        self.operator = operator
        if not isinstance(translation, Translation):
            raise TypeError('expected a Translation')
        if translation.group.space != operator.left_space:
            raise ValueError("Translation group does not act on the "
                             "operator's left Hilbert space.")


class Symmetrized(Operator):
    """Conjugate with all operators from a symmetry group.

    Given an operator 'A', 'Symmetrized(A)' represents the operator

        A' = ∑ g⁻¹ A g,

    where the sum runs over all elements of a given symmetry group.

    Attributes
    ----------
    group : OperatorGroup
    operator : Operator
    """

    def __init__(self, group, operator):
        self.group = group
        self.operator = operator
        if (operator.left_space != group.space or
            operator.right_space != group.space):
                raise ValueError('This group has no action on this operator')


class HalfSymmetrized(Operator):
    """Conjugate with a subset of operators from a translation group.

    Given an operator 'A', 'Symmetrized(A)' represents the operator

        A' = ∑ t⁻¹ A t,

    where the sum runs over all elements of a translation group, 'T'
    whose components satisfy the constraint:

        ∑ a_j t_j ≤ c

    where a translation group element is written '(t_0, t_1, ..., t_n)'.
    and the 'a_j' and 'c' specify the constraint.

    Attributes
    ----------
    group : TranslationGroup
    operator : Operator
    constraint : tuple of int
        '(c, a_0, a_1, ...)'.
    """

class Projected(Operator):
    """Product with a diagonal projector.

    Given an operator 'A', 'Projected(A)' represents
    either:

        PAP,

    if 'include_hoppings == False', or

        PA + AP - PAP

    if 'include_hoppings == True', where 'P' is a diagonal projector.

    Attributes
    ----------
    operator : Operator
    projector : Projector, Symmetrized(Projector), HalfSymmetrized(Projector)
    include_hoppings : bool
    """

## Operator Groups
#  These will be used to define symmetry operators

class OperatorGroup(metaclass=abc.ABCMeta):
    """A group of operators acting on a Hilbert space."""


class TrivialGroup(OperatorGroup):
    """The trivial operator group

    Attributes
    ----------
    space : HilbertSpace
    """


class TranslationGroup(OperatorGroup):
    """A group of operators that translates tags.

    Operators in this group are infinite permutation matrices, but can be
    better understood by their action (translation) on tags.

    Attributes
    ----------
    space : HilbertSpace
    generators : ndarray of generators
        Linearly independent tag translations. Each row is a tag
        translation on the *infinite* part of 'space'.
    """

    def __init__(self, space, generators):
        self.space = space

        if not np.allclose(generators, np.round(generators), rtol=0, atol=1e-8):
            raise ValueError('Generators must be a 2D integer array')

        self.generators = generators = np.array(np.round(generators), int)
        if len(generators.shape) != 2:
            raise ValueError('Generators must be a 2D integer array')
        if generators.shape[1] != space.num_inf_dims:
            raise ValueError('Generators must span the infinite dimensions '
                             'of the Hilbert space')
        if np.linalg.matrix_rank(generators) < len(generators):
            raise ValueError('Generators must be linearly independent')
        self._g_inv = np.linalg.pinv(self.generators)

    @classmethod
    def from_realspace(cls, space, periods):
        prim_vecs, _ = space.infinite_basis

        gens = periods @ np.linalg.pinv(prim_vecs)
        if not np.allclose(gens, np.round(gens), rtol=0, atol=1e-8):
            raise ValueError('"Periods" for TranslationGroup must match the '
                             'symmetry of underlying lattice.')

        return cls(space, np.round(gens))

    @property
    def periods(self):
        prim_vecs, _ = self.space.infinite_basis
        return np.dot(self.generators, prim_vecs)

    @property
    def reciprocal_periods(self):
        return np.linalg.pinv(self.periods).T

    def momentum_to_lattice(self, k):
        """Return momentum vector in reciprocal basis of periods."""
        k, residuals = np.linalg.lstsq(
            self.reciprocal_periods.T, k, rcond=None
        )[:2]
        if np.any(abs(residuals) > 1e-7):
            raise RuntimeError("Requested momentum doesn't correspond "
                               "to any lattice momentum.")
        return k

    @property
    def brillouin_zone(self):
        prim_vecs, _ = self.space.infinite_basis
        lat_ndim, space_ndim = prim_vecs.shape

        # Get lattice points that neighbor the origin, in basis of lattice vecs
        reduced_vecs, transf = lll.lll(self.reciprocal_periods)
        neighbors = np.dot(lll.voronoi(reduced_vecs), transf)
        # Add the origin to these points.
        klat_points = np.concatenate(([[0] * lat_ndim], neighbors))
        # Transform to cartesian coordinates and rescale.
        # Will be used in 'outside_bz' function, later on.
        klat_points = 2 * np.pi * np.dot(klat_points, self.reciprocal_periods)
        # Calculate the Voronoi cell vertices
        vor = scipy.spatial.Voronoi(klat_points)
        around_origin = vor.point_region[0]
        bz_vertices = vor.vertices[vor.regions[around_origin]]
        # extract bounding box
        k_max = np.max(np.abs(bz_vertices), axis=0)

        return k_max, klat_points

    def which(self, tags):
        """Return the domain in which each of the tags"""
        inf_part = tags[:, :self.space.num_inf_dims]
        # 'floor' to round towards -inf; properly identifies tags in (-1, 0]
        return np.floor(np.dot(inf_part, self._g_inv)).astype(int)

    def to_fd(self, tags):
        return self.act(-self.which(tags), tags)

    def act(self, vectors, tags):
        n = self.space.num_inf_dims
        translated_tags = np.array(tags)
        translated_tags[:, :n] += np.dot(vectors, self.generators)
        return translated_tags


class Translation(SimpleOperator):
    """A single operator from a TranslationGroup.

    Attributes
    ----------
    group : TranslationGroup
    vector : array of int
        Translation vector in the basis of 'group.generators'.
    """

    def act(self, tags):
        return self.group.act(self.vector, tags)
