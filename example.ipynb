{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Kwant 2 formats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hilbert spaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from math import inf\n",
    "\n",
    "from hilbert_space import SiteArray, Simple, Product"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 2 kinds of \"simple\" Hilbert space that we will consider:\n",
    "\n",
    "+ Infinite dimensional, with `d` directions. An element is labelled\n",
    "  by an element of `ℤ^d`.\n",
    "+ Finite (`n`) dimensional (e.g. spin 1/2). An element is labelled by\n",
    "  an integer `0 <= i < n`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Could be a 2D lattice (but we haven't specified what yet!)\n",
    "lat = Simple(inf, 'x') * Simple(inf, 'y')\n",
    "# Could be a Landau level\n",
    "landau = Simple(inf, name='landau')\n",
    "# Could be a spin\n",
    "spin = Simple(2, name='spin')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "l1 = Simple(inf, 'x') * Simple(inf, 'y') * Simple(inf, 'z')\n",
    "l2 = Simple(inf, 'x') * Simple(inf, 'y')\n",
    "l3 = Simple(inf, 'x') * Simple(inf, 'z')\n",
    "\n",
    "l1.has_subspace(l2), l1.has_subspace(l3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may construct \"composite\" Hilbert spaces that are direct products of these basic types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = lat * spin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because we wish to uniquely label all elements of the Hilbert space, a given simple Hilbert space may only appear once in a given composite Hilbert space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    spin * spin\n",
    "except ValueError as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also determine whether one hilbert space is a subspace of another:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H.has_subspace(lat), H.has_subspace(lat * spin)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spin.has_subspace(lat), spin.has_subspace(H)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Site Arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We refer to basis vectors of the Hilbert space as *sites*.\n",
    "\n",
    "It is wasteful to store the tag and space separately every time, so we may define arrays of sites that belong to the same Hilbert space: `SiteArray`s."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rectangle = SiteArray(lat, np.mgrid[0:500, 0:1000].transpose().reshape(-1, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import kwoperator as op"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## FiniteSupport operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This class of operators is an explicit collection of matrix elements.\n",
    "\n",
    "We may build such an operator by providing 2 `SiteArray`s and a sequence of values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const = op.FiniteSupport.from_list(rectangle, rectangle, np.arange(len(rectangle)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then call the operator to get a COO matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operator groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may declare a *group* of operators with `TranslationGroup`. We may only create translation groups over infinite Hilbert spaces, and by definition the operators act in a way that is equivalent to a translation of site tags."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = op.TranslationGroup(lat, [[1, 0], [0, 1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is an affine transformation between tags in `lat` and realspace then `T` is a *representation of a translational space group*.\n",
    "\n",
    "It is, however, important to remember that `TranslationGroup` really is defined as a group of *operators on a Hilbert space*; we have not yet formally defined any notion of realspace translations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Symmetrized operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have defined a group of operators we may *symmetrize* another operator with respect to this group:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "to_sites = SiteArray(lat, [\n",
    "    [0, 0],\n",
    "    [1, 0],\n",
    "    [0, 1],\n",
    "])\n",
    "\n",
    "from_sites = SiteArray(lat, [\n",
    "    [0, 0],\n",
    "    [0, 0],\n",
    "    [0, 0],\n",
    "])\n",
    "\n",
    "values = [4, -1, -1]\n",
    "\n",
    "\n",
    "H_f = op.FiniteSupport.from_list(to_sites, from_sites, values)\n",
    "H_p = op.Symmetrized(T, H_f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H_p"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Py3k",
   "language": "python",
   "name": "py3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
