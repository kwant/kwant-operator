import itertools as it
import functools as ft
import numpy as np
from math import pi

import holoviews as hv

from adapters import KSpaceAdapter


def band_structure(kspace_hamiltonian, npoints=100):
    assert isinstance(kspace_hamiltonian, KSpaceAdapter)
    h_k = ft.partial(kspace_hamiltonian, sparse=False)
    k_1d = np.linspace(-2*pi, 2*pi, npoints)
    dims = len(kspace_hamiltonian.sym.generators)

    if dims  == 1:
        spectrum = np.array([np.linalg.eigvalsh(h_k(k)) for k in k_1d])
        kwargs = dict(kdims=['k0'], vdims=['Energy'])
        plots = [hv.Curve((k_1d, s), **kwargs) for s in spectrum.swapaxes(0, -1)]
    elif dims == 2:
        k_2d = it.product(k_1d, repeat=2)
        spectrum = np.array([np.linalg.eigvalsh(h_k(k)) for k in k_2d])
        spectrum = spectrum.reshape(npoints, npoints, -1)
        kwargs = dict(kdims=['k0', 'k1'], vdims=['Energy'])
        plots = [hv.Surface((k_1d, k_1d, s), **kwargs) for s in spectrum.swapaxes(0, -1)]
    else:
        raise ValueError(f'Cannot plot {dims}-D bandstructures')

    return hv.Overlay(plots)
